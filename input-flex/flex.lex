%option yylineno
%option reentrant bison-bridge bison-locations
/*%option hex*/
/*%option nodefault*/
/*%option nounistd*/
%option full

%{
#include "../include/ed-init.hpp"

#include "bison.hpp"
#include <cstring>
#include <map>
#include <string>

namespace /*anon*/ {

constexpr auto TAB_SIZE = size_t(4);

std::map<std::string, int> _parenTokens = {
    {"{", T_LEFT_CURLY},
    {"(", T_LEFT_PAREN},
    {"[", T_LEFT_SQUARE},
    {"{", T_RIGHT_CURLY},
    {"(", T_RIGHT_PAREN},
    {"[", T_RIGHT_SQUARE},
};


std::map<std::string, int> _keywordTokens = {
    {"abstract", T_ABSTRACT},
    {"alias", T_ALIAS},
    {"const", T_CONST},
    {"def", T_DEF},
    {"extern", T_EXTERN},
    {"let", T_LET},
    {"mut", T_MUT},
    {"overload", T_OVERLOAD},
    {"override", T_OVERRIDE},
    {"package", T_PACKAGE},
    {"private", T_PRIVATE},
    {"protected", T_PROTECTED},
    {"public", T_PUBLIC},
    {"static", T_STATIC},
    {"template", T_TEMPLATE},
    {"var", T_VAR},
    {"volatile", T_VOLATILE},
};

std::map<std::string, int> _miscTokens = {
    {"[", T_IDENTIFIER},
};

std::map<std::string, int> _operatorTokens = {
    {":", T_COLON},
    {".", T_DOT},
    {"=", T_EQUAL},
};

std::string _currentStringDelimiter;

auto getToken(const std::string & str) -> int
{
    const auto it0 = _parenTokens.find(str);
    if (it0 != _parenTokens.end()) return it0->second;

    const auto it1 = _keywordTokens.find(str);
    if (it1 != _keywordTokens.end()) return it0->second;

    const auto it2 = _miscTokens.find(str);
    if (it2 != _miscTokens.end()) return it0->second;

    const auto it3 = _operatorTokens.find(str);
    if (it3 != _operatorTokens.end()) return it0->second;

    return T_IDENTIFIER;
}

} // namespace
%}

/* *****************************************************************************
 * Definitions
 * ************************************************************************** */

comment          [\#]

punctuation      [\.\:\,\@]
operator         [\+\-\*\/\\\|\=\<\>\?\&\%\$\!\~\^\`]+

string_delimeter ("""|'''|"|')

identifier       _*[A-Za-z][A-Za-z0-9_]*

binary_number    _*0[bB]_*[01][01_]*
hex_number       _*0[xX]_*[0-9a-fA-F][0-9a-fA-F_]*
octal_number     _*0_*[0-7][0-7_]*
decimal_number   _*[0-9][0-9_]*(.[0-9_]+)?(eE[+-][0-9_]+)?

WC               [ \b\f\r\v]+
TAB              [\t]
NEWLINE          \n

/*
 * Exclusive start condition: %x
 * Inclusive start condition: %s
 */
%x SKIP_TO_EOL
%x STRING

/*
* %option never-interactive
* %option noyywrap
*/

/* *****************************************************************************
 * Rules
 * ************************************************************************** */
%%

<<EOF>> {
    //yyterminate();
    return 0;
}

{WC} {
    yycolumn += static_cast<int>(strlen(yytext));
}
{TAB} {
    yycolumn += static_cast<int>(strlen(yytext) * TAB_SIZE);
}
{NEWLINE} {
    yycolumn = 1;
    yylineno += 1;
}

<SKIP_TO_EOL>.*$ {
    yycolumn = 1;
    yylineno += 1;
    BEGIN(INITIAL);
}

\{ {
    yycolumn += strlen(yytext);
    return T_LEFT_CURLY;
}
\} {
    yycolumn += strlen(yytext);
    return T_RIGHT_CURLY;
}
\( {
    yycolumn += strlen(yytext);
    return T_LEFT_PAREN;
}
\) {
    yycolumn += strlen(yytext);
    return T_RIGHT_PAREN;
}
\[ {
    yycolumn += strlen(yytext);
    return T_LEFT_SQUARE;
}
\] {
    yycolumn += strlen(yytext);
    return T_RIGHT_SQUARE;
}

{comment} {
    yycolumn += strlen(yytext);
    BEGIN(SKIP_TO_EOL);
}

{punctuation} {
    yycolumn += strlen(yytext);
    return getToken(yytext);
}
{operator} {
    yycolumn += strlen(yytext);
    return getToken(yytext);
}

{binary_number} {
    yycolumn += strlen(yytext);
    yylval->StringValue = strdup(yytext);
    return T_NUMBER;
}
{hex_number} {
    yycolumn += strlen(yytext);
    yylval->StringValue = strdup(yytext);
    return T_NUMBER;
}
{octal_number} {
    yycolumn += strlen(yytext);
    yylval->StringValue = strdup(yytext);
    return T_NUMBER;
}
{decimal_number} {
    yycolumn += strlen(yytext);
    yylval->StringValue = strdup(yytext);
    return T_NUMBER;
}

{identifier} {
    yycolumn += strlen(yytext);
    yylval->StringValue = strdup(yytext);
    return getToken(yytext);
}

{string_delimeter} {
    yycolumn += strlen(yytext);
    _currentStringDelimiter = yytext;
    yylval->StringValue = strdup(yytext);
    BEGIN(STRING);
    return T_STRING_DELIMITER;
}

<STRING>(\\\\)*\\{string_delimeter} {
    yycolumn += strlen(yytext);
    yylval->StringValue = strdup(yytext);
    return T_STRING_CONTENT;
}

<STRING>{string_delimeter} {
    yycolumn += strlen(yytext);
    yylval->StringValue = strdup(yytext);
    BEGIN(INITIAL);
    return T_STRING_DELIMITER;
}

<STRING>{NEWLINE} {
    yycolumn = 1;
    yylineno += 1;
    yylval->StringValue = strdup(yytext);
    return T_STRING_CONTENT;
}

<STRING>. {
    yycolumn += strlen(yytext);
    yylval->StringValue = strdup(yytext);
    return T_STRING_CONTENT;
}

%%
