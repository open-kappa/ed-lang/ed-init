#ifndef EDINIT_FLEX_FLEX_HPP
#define EDINIT_FLEX_FLEX_HPP

#include <string>
#include <vector>

namespace edinit::flex {

struct FlexToken
{
    std::string value;
    std::string file;
    size_t line;
    size_t column;
};

class Flex final
{
public:
    Flex();
    ~Flex() noexcept;

    [[nodiscard]] auto tokenizeFile(const std::string & file) -> std::vector<FlexToken>;
    [[nodiscard]] auto tokenizeString(const std::string & str, const std::string & file) -> std::vector<FlexToken>;
    auto clear() noexcept -> void;

private:
    void * _flex;

    Flex(const Flex & other) = delete;
    Flex(Flex && other) noexcept = delete;
    auto operator =(const Flex & other) -> Flex = delete;
    auto operator =(Flex && other) noexcept -> Flex = delete;
};

} // namespace edinit::flex

#endif
