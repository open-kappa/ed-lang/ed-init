#ifndef EDINIT_PARSER_PARSER_HPP
#define EDINIT_PARSER_PARSER_HPP

#include "../ast.hpp"
#include <memory>
#include <string>

namespace edinit::parser {

class Parser final
{
public:
    using BindModifier = edinit::ast::BindModifier;
    using Declaration = edinit::ast::Declaration;
    using DeclarationKind = edinit::ast::DeclarationKind;
    using Identifier = edinit::ast::Identifier;
    using Number = edinit::ast::Number;
    using ParameterAssign = edinit::ast::ParameterAssign;
    using ParameterAssignList = edinit::ast::ParameterAssignList;
    using ParameterDeclaration = edinit::ast::ParameterDeclaration;
    using ParameterDeclarationList = edinit::ast::ParameterDeclarationList;
    using Scope = edinit::ast::Scope;
    using Statement = edinit::ast::Statement;
    using String = edinit::ast::String;
    using TemplateDeclaration = edinit::ast::TemplateDeclaration;
    using TemplateParameterAssign = edinit::ast::TemplateParameterAssign;
    using Value = edinit::ast::Value;
    using VisibilityModifier = edinit::ast::VisibilityModifier;

    Parser();
    ~Parser();
    Parser(const Parser & other);
    Parser(Parser && other) noexcept;
    auto operator =(const Parser & other) -> Parser &;
    auto operator =(Parser && other) noexcept -> Parser &;
    auto swap(Parser & other) noexcept -> void;


    [[nodiscard]] auto declaration(
        TemplateDeclaration * const templateDeclaration,
        const VisibilityModifier visibilityModifier,
        const BindModifier bindModifier,
        const DeclarationKind declarationKind,
        Identifier * const declarationName,
        Value * const type,
        Value * const initialValue
    ) const -> Declaration *;

    [[nodiscard]] auto declarationType(Identifier * const type) const noexcept -> Identifier *;

    [[nodiscard]] auto identifier(const std::string & value) const -> Identifier *;

    [[nodiscard]] auto nestedIdentifier(Identifier * const type, TemplateParameterAssign * const templateParams) const -> Identifier *;
    [[nodiscard]] auto nestedIdentifier(Identifier * prefix, Identifier * const type, TemplateParameterAssign * const templateParams) const -> Identifier *;

    [[nodiscard]] auto number(const std::string & value) const -> Number *;

    [[nodiscard]] auto parameterAssign(Value * const value) const -> ParameterAssign *;
    [[nodiscard]] auto parameterAssign(Identifier * const name, Value * const value) const -> ParameterAssign *;

    [[nodiscard]] auto parameterAssignList() const -> ParameterAssignList *;
    [[nodiscard]] auto parameterAssignList(ParameterAssign * const paramAssign) const -> ParameterAssignList *;
    [[nodiscard]] auto parameterAssignList(ParameterAssign * const paramAssign, ParameterAssignList * const paramAssignList) const -> ParameterAssignList *;

    [[nodiscard]] auto parameterDeclaration(
        TemplateDeclaration * const templateDeclaration,
        const VisibilityModifier visibilityModifier,
        const BindModifier bindModifier,
        const DeclarationKind declarationKind,
        Identifier * const declarationName,
        Value * const type,
        Value * const initialValue
    ) const -> ParameterDeclaration *;

    [[nodiscard]] auto parameterDeclarationList() const -> ParameterDeclarationList *;
    [[nodiscard]] auto parameterDeclarationList(ParameterDeclaration * const param) const -> ParameterDeclarationList *;
    [[nodiscard]] auto parameterDeclarationList(ParameterDeclaration * const param, ParameterDeclarationList * const list) const -> ParameterDeclarationList *;

    [[nodiscard]] auto scope() const -> Scope *;
    [[nodiscard]] auto scope(Declaration * const decl, Scope * const scope) const -> Scope *;
    [[nodiscard]] auto scope(Statement * const decl, Scope * const scope) const -> Scope *;

    [[nodiscard]] auto statement(Statement * const value) const noexcept -> Statement *;

    [[nodiscard]] auto string(const std::string & delimiter, String * const value) const -> String *;
    [[nodiscard]] auto stringContent() const -> String *;
    [[nodiscard]] auto stringContent(String * const other, const std::string & value) const -> String *;

    [[nodiscard]] auto templateDeclaration(const bool isExtern, ParameterDeclarationList * const list) const -> TemplateDeclaration *;

    [[nodiscard]] auto templateParameterAssign() const -> TemplateParameterAssign *;
    [[nodiscard]] auto templateParameterAssign(ParameterAssignList * const list) const -> TemplateParameterAssign *;

    [[nodiscard]] auto tokenBindNone() const noexcept -> BindModifier;
    [[nodiscard]] auto tokenAbstract() const noexcept -> BindModifier;
    [[nodiscard]] auto tokenOverload() const noexcept -> BindModifier;
    [[nodiscard]] auto tokenOverride() const noexcept -> BindModifier;
    [[nodiscard]] auto tokenStatic() const noexcept -> BindModifier;
    [[nodiscard]] auto tokenVolatile() const noexcept -> BindModifier;

    [[nodiscard]] auto tokenKindNone() const noexcept -> DeclarationKind;
    [[nodiscard]] auto tokenLet() const noexcept -> DeclarationKind;
    [[nodiscard]] auto tokenDef() const noexcept -> DeclarationKind;
    [[nodiscard]] auto tokenConst() const noexcept -> DeclarationKind;
    [[nodiscard]] auto tokenMut() const noexcept -> DeclarationKind;
    [[nodiscard]] auto tokenVar() const noexcept -> DeclarationKind;
    [[nodiscard]] auto tokenAlias() const noexcept -> DeclarationKind;

    [[nodiscard]] auto tokenVisibilityNone() const noexcept -> VisibilityModifier;
    [[nodiscard]] auto tokenPublic() const noexcept -> VisibilityModifier;
    [[nodiscard]] auto tokenProject() const noexcept -> VisibilityModifier;
    [[nodiscard]] auto tokenProtected() const noexcept -> VisibilityModifier;
    [[nodiscard]] auto tokenPrivate() const noexcept -> VisibilityModifier;


private:
};

auto swap(Parser & i1, Parser & i2) noexcept -> void;

using ParserPtr = std::shared_ptr<Parser>;

} // namespace edinit::parser

#endif
