#ifndef EDINIT_AST_HPP
#define EDINIT_AST_HPP

namespace edinit {
/// Namespace for AST
namespace ast {
} // namespace ast
} // namespace edinit

#include "ast/Assign.hpp" // IWYU pragma: keep
#include "ast/BindModifier.hpp" // IWYU pragma: keep
#include "ast/Declaration.hpp" // IWYU pragma: keep
#include "ast/DeclarationKind.hpp" // IWYU pragma: keep
#include "ast/Identifier.hpp" // IWYU pragma: keep
#include "ast/List.hpp" // IWYU pragma: keep
#include "ast/Number.hpp" // IWYU pragma: keep
#include "ast/Object.hpp" // IWYU pragma: keep
#include "ast/ParameterDeclaration.hpp" // IWYU pragma: keep
#include "ast/Scope.hpp" // IWYU pragma: keep
#include "ast/Statement.hpp" // IWYU pragma: keep
#include "ast/String.hpp" // IWYU pragma: keep
#include "ast/TemplateDeclaration.hpp" // IWYU pragma: keep
#include "ast/TemplateParameterAssign.hpp" // IWYU pragma: keep
#include "ast/Value.hpp" // IWYU pragma: keep
#include "ast/VisibilityModifier.hpp" // IWYU pragma: keep
#include "ast/Visitor.hpp" // IWYU pragma: keep

#endif
