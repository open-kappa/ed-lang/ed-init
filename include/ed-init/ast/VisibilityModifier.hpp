#ifndef EDINIT_AST_VISIBILITYMODIFIER_HPP
#define EDINIT_AST_VISIBILITYMODIFIER_HPP

namespace edinit::ast {

enum class VisibilityModifier
{
    NONE,
    PUBLIC,
    PACKAGE,
    PROTECTED,
    PRIVATE
};

} // namespace edinit::ast

#endif
