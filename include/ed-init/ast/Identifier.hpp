#ifndef EDINIT_AST_IDENTIFIER_HPP
#define EDINIT_AST_IDENTIFIER_HPP

#include "Value.hpp"
#include <string>

namespace edinit::ast {

class Identifier final:
    public Value
{
public:
    Identifier();
    ~Identifier() override;
    Identifier(const Identifier & other);
    Identifier(Identifier && other) noexcept;
    auto operator =(const Identifier & other) -> Identifier &;
    auto operator =(Identifier && other) noexcept -> Identifier &;
    auto swap(Identifier & other) noexcept -> void;

    auto clone() -> Identifier * override;
    auto acceptPreVisitor(Visitor & visitor) -> VisitExit override;
    auto acceptPostVisitor(Visitor & visitor) -> VisitExit override;
    auto children() -> Children override;

    auto name(const std::string & value) noexcept -> void;
    auto name() const noexcept -> const std::string &;

    auto prefix(const ValuePtr & value) noexcept -> void;
    auto prefix() const noexcept -> ValuePtr;

private:
    std::string _name;
    ValuePtr _prefix;
};

auto swap(Identifier & i1, Identifier & i2) noexcept -> void;

using IdentifierPtr = std::shared_ptr<Identifier>;

} // namespace edinit::ast

#endif
