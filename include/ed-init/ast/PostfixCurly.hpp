#ifndef EDINIT_AST_POSTFIXCURLY_HPP
#define EDINIT_AST_POSTFIXCURLY_HPP

#include "Value.hpp"
#include <string>

namespace edinit::ast {

class PostfixCurly final:
    public Value
{
public:
    PostfixCurly();
    ~PostfixCurly() override;
    PostfixCurly(const PostfixCurly & other);
    PostfixCurly(PostfixCurly && other) noexcept;
    auto operator =(const PostfixCurly & other) -> PostfixCurly &;
    auto operator =(PostfixCurly && other) noexcept -> PostfixCurly &;
    auto swap(PostfixCurly & other) noexcept -> void;

    auto acceptPreVisitor(Visitor & visitor) -> VisitExit override;
    auto acceptPostVisitor(Visitor & visitor) -> VisitExit override;
    auto children() -> Children override;

    auto prefix(ValuePtr value) noexcept -> void;
    auto prefix() const noexcept -> ValuePtr;

private:
    ValuePtr _prefix;
};

auto swap(PostfixCurly & i1, PostfixCurly & i2) noexcept -> void;

using PostfixCurlyPtr = std::shared_ptr<PostfixCurly>;

} // namespace edinit::ast

#endif
