#ifndef EDINIT_AST_LIST_HPP
#define EDINIT_AST_LIST_HPP

#include <list>
#include <memory>

namespace edinit::ast {

template<typename T>
using List = std::list<std::shared_ptr<T>>;

} // namespace edinit::ast

#endif
