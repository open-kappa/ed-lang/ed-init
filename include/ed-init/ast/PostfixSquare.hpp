#ifndef EDINIT_AST_POSTFIXSQUARE_HPP
#define EDINIT_AST_POSTFIXSQUARE_HPP

#include "Value.hpp"
#include <string>

namespace edinit::ast {

class PostfixSquare final:
    public Value
{
public:
    PostfixSquare();
    ~PostfixSquare() override;
    PostfixSquare(const PostfixSquare & other);
    PostfixSquare(PostfixSquare && other) noexcept;
    auto operator =(const PostfixSquare & other) -> PostfixSquare &;
    auto operator =(PostfixSquare && other) noexcept -> PostfixSquare &;
    auto swap(PostfixSquare & other) noexcept -> void;

    auto acceptPreVisitor(Visitor & visitor) -> VisitExit override;
    auto acceptPostVisitor(Visitor & visitor) -> VisitExit override;
    auto children() -> Children override;

    auto prefix(ValuePtr value) noexcept -> void;
    auto prefix() const noexcept -> ValuePtr;

private:
    ValuePtr _prefix;
};

auto swap(PostfixSquare & i1, PostfixSquare & i2) noexcept -> void;

using PostfixSquarePtr = std::shared_ptr<PostfixSquare>;

} // namespace edinit::ast

#endif
