#ifndef EDINIT_AST_PARAMETERDECLARATION_HPP
#define EDINIT_AST_PARAMETERDECLARATION_HPP

#include "Declaration.hpp"
#include "Value.hpp"
#include <string>

namespace edinit::ast {

class ParameterDeclaration final:
    public Declaration
{
public:
    ParameterDeclaration();
    ~ParameterDeclaration() override;
    ParameterDeclaration(const ParameterDeclaration & other);
    ParameterDeclaration(ParameterDeclaration && other) noexcept;
    auto operator =(const ParameterDeclaration & other) -> ParameterDeclaration &;
    auto operator =(ParameterDeclaration && other) noexcept -> ParameterDeclaration &;
    auto swap(ParameterDeclaration & other) noexcept -> void;

    auto clone() -> ParameterDeclaration * override;
    auto acceptPreVisitor(Visitor & visitor) -> VisitExit override;
    auto acceptPostVisitor(Visitor & visitor) -> VisitExit override;
    auto children() -> Children override;

    auto defaultValue(const ValuePtr & value) noexcept -> void;
    auto defaultValue() noexcept -> ValuePtr &;

    auto type(const ValuePtr & value) noexcept -> void;
    auto type() noexcept -> ValuePtr &;

    auto name(const std::string & value) -> void;
    auto name() const noexcept -> const std::string &;

    // @TODO
    // def, const, mut, ...
    // byValue, byRef, byName, ...

private:
    ValuePtr _defaultValue;
    ValuePtr _type;
    std::string _name;
};

auto swap(ParameterDeclaration & i1, ParameterDeclaration & i2) noexcept -> void;

using ParameterDeclarationPtr = std::shared_ptr<ParameterDeclaration>;

using ParameterDeclarationList = List<ParameterDeclaration>;

} // namespace edinit::ast

#endif
