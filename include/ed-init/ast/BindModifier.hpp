#ifndef EDINIT_AST_BINDMODIFIER_HPP
#define EDINIT_AST_BINDMODIFIER_HPP

namespace edinit::ast {

enum class BindModifier
{
    NONE,
    ABSTRACT,
    OVERLOAD,
    OVERRIDE,
    STATIC,
    VOLATILE
};

} // namespace edinit::ast

#endif
