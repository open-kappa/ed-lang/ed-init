#ifndef EDINIT_AST_TEMPLATEDECLARATION_HPP
#define EDINIT_AST_TEMPLATEDECLARATION_HPP

#include "Declaration.hpp"
#include "ParameterDeclaration.hpp"

namespace edinit::ast {

class TemplateDeclaration final:
    public Declaration
{
public:
    TemplateDeclaration();
    ~TemplateDeclaration() override;
    TemplateDeclaration(const TemplateDeclaration & other);
    TemplateDeclaration(TemplateDeclaration && other) noexcept;
    auto operator =(const TemplateDeclaration & other) -> TemplateDeclaration &;
    auto operator =(TemplateDeclaration && other) noexcept -> TemplateDeclaration &;
    auto swap(TemplateDeclaration & other) noexcept -> void;

    [[nodiscard]] auto isExtern() const noexcept -> bool;
    auto isExtern(const bool value) noexcept -> void;

    [[nodiscard]] auto parameters() const noexcept -> ParameterDeclarationPtr;
    auto parameters(ParameterDeclarationPtr value) noexcept -> void;

private:
    bool _isExtern;


};

using TemplateDeclarationPtr = std::shared_ptr<TemplateDeclaration>;

} // namespace edinit::ast

#endif
