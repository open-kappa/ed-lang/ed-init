#ifndef EDINIT_AST_ASSIGN_HPP
#define EDINIT_AST_ASSIGN_HPP

#include "Identifier.hpp"
#include "Statement.hpp"
#include "Value.hpp"

namespace edinit::ast {

/// Maps assignments.
/// It can be:
/// - An assign statement
/// - A parameter assign
/// - A template parameter assign
class Assign final:
    public Statement
{
public:
    Assign();
    ~Assign() override;
    Assign(const Assign & other);
    Assign(Assign && other) noexcept;
    auto operator =(const Assign & other) -> Assign &;
    auto operator =(Assign && other) noexcept -> Assign &;
    auto swap(Assign & other) noexcept -> void;

    auto clone() -> Assign * override;

    auto acceptPreVisitor(Visitor & visitor) -> VisitExit override;
    auto acceptPostVisitor(Visitor & visitor) -> VisitExit override;
    auto children() -> Children override;

    auto source(const ValuePtr & value) -> void;
    auto source() noexcept -> ValuePtr;

    auto target(const IdentifierPtr & value) -> void;
    auto target() noexcept -> IdentifierPtr &;

    auto op(const std::string & op) -> void;
    auto op() const noexcept -> const std::string &;

private:
    ValuePtr _source;
    IdentifierPtr _target;
    std::string _op;
};

auto swap(Assign & i1, Assign & i2) noexcept -> void;

using AssignPtr = std::shared_ptr<Assign>;

using AssignList = List<Assign>;

} // namespace edinit::ast

#endif
