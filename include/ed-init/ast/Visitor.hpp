#ifndef EDINIT_AST_VISITOR_HPP
#define EDINIT_AST_VISITOR_HPP

#include <memory>

namespace edinit::ast {
class Declaration;
class Identifier;
class Number;
class Object;
class ParameterAssign;
class ParameterDeclaration;
class Scope;
class Statement;
class String;
class TemplateDeclaration;
class TemplateParameterAssign;
class Value;
} // namespace edinit::ast

namespace edinit::ast {

enum class VisitExit
{
    CONTINUE,
    SKIP_CHILDREN,
    STOP
};

class Visitor
{
public:
    using ObjectPtr = std::shared_ptr<Object>;

    Visitor();
    virtual ~Visitor() = 0;

    auto execDeepVisit(const ObjectPtr & root) -> void;
    auto execBreadthVisit(const ObjectPtr & root) -> void;

    virtual auto preVisit(Declaration & obj) -> VisitExit;
    virtual auto preVisit(Identifier & obj) -> VisitExit;
    virtual auto preVisit(Number & obj) -> VisitExit;
    virtual auto preVisit(Object & obj) -> VisitExit;
    virtual auto preVisit(ParameterAssign & obj) -> VisitExit;
    virtual auto preVisit(ParameterDeclaration & obj) -> VisitExit;
    virtual auto preVisit(Scope & obj) -> VisitExit;
    virtual auto preVisit(Statement & obj) -> VisitExit;
    virtual auto preVisit(String & obj) -> VisitExit;
    virtual auto preVisit(TemplateDeclaration & obj) -> VisitExit;
    virtual auto preVisit(TemplateParameterAssign & obj) -> VisitExit;
    virtual auto preVisit(Value & obj) -> VisitExit;

    virtual auto postVisit(Declaration & obj) -> VisitExit;
    virtual auto postVisit(Identifier & obj) -> VisitExit;
    virtual auto postVisit(Number & obj) -> VisitExit;
    virtual auto postVisit(Object & obj) -> VisitExit;
    virtual auto postVisit(ParameterAssign & obj) -> VisitExit;
    virtual auto postVisit(ParameterDeclaration & obj) -> VisitExit;
    virtual auto postVisit(Scope & obj) -> VisitExit;
    virtual auto postVisit(Statement & obj) -> VisitExit;
    virtual auto postVisit(String & obj) -> VisitExit;
    virtual auto postVisit(TemplateDeclaration & obj) -> VisitExit;
    virtual auto postVisit(TemplateParameterAssign & obj) -> VisitExit;
    virtual auto postVisit(Value & obj) -> VisitExit;

private:
    Visitor(const Visitor & other) = delete;
    Visitor(Visitor && other) noexcept = delete;
    auto operator =(const Visitor & other) -> Visitor & = delete;
    auto operator =(Visitor && other) noexcept -> Visitor & = delete;
};

using VisitorPtr = std::shared_ptr<Visitor>;

} // namespace edinit::ast

#endif
