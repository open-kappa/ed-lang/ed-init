#ifndef EDINIT_AST_DECLARATION_HPP
#define EDINIT_AST_DECLARATION_HPP

#include "Object.hpp"

namespace edinit::ast {

class Declaration:
    public Object
{
public:
    ~Declaration() override = 0;

    auto clone() -> Declaration * override = 0;
    auto children() -> Children override;

protected:
    Declaration();
    Declaration(const Declaration & other);
    Declaration(Declaration && other) noexcept;
    auto swap(Declaration & other) noexcept -> void;

private:
    auto operator =(const Declaration & other) -> Declaration & = delete;
    auto operator =(Declaration && other) noexcept -> Declaration & = delete;
};

using DeclarationPtr = std::shared_ptr<Declaration>;

} // namespace edinit::ast

#endif
