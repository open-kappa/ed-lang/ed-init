#ifndef EDINIT_AST_POSTFIXPAREN_HPP
#define EDINIT_AST_POSTFIXPAREN_HPP

#include "Value.hpp"
#include <string>

namespace edinit::ast {

class PostfixParen final:
    public Value
{
public:
    PostfixParen();
    ~PostfixParen() override;
    PostfixParen(const PostfixParen & other);
    PostfixParen(PostfixParen && other) noexcept;
    auto operator =(const PostfixParen & other) -> PostfixParen &;
    auto operator =(PostfixParen && other) noexcept -> PostfixParen &;
    auto swap(PostfixParen & other) noexcept -> void;

    auto acceptPreVisitor(Visitor & visitor) -> VisitExit override;
    auto acceptPostVisitor(Visitor & visitor) -> VisitExit override;
    auto children() -> Children override;

    auto prefix(ValuePtr value) noexcept -> void;
    auto prefix() const noexcept -> ValuePtr;

private:
    ValuePtr _prefix;
};

auto swap(PostfixParen & i1, PostfixParen & i2) noexcept -> void;

using PostfixParenPtr = std::shared_ptr<PostfixParen>;

} // namespace edinit::ast

#endif
