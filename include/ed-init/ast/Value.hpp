#ifndef EDINIT_AST_VALUE_HPP
#define EDINIT_AST_VALUE_HPP

#include "Object.hpp"

namespace edinit::ast {

class Value:
    public Object
{
public:
    ~Value() override = 0;

    auto clone() -> Value * override = 0;
    auto children() -> Children override;

protected:
    Value();
    Value(const Value & other);
    Value(Value && other) noexcept;
    auto swap(Value & other) noexcept -> void;

private:
    auto operator =(const Value & other) -> Value & = delete;
    auto operator =(Value && other) noexcept -> Value & = delete;
};

using ValuePtr = std::shared_ptr<Value>;

} // namespace edinit::ast

#endif
