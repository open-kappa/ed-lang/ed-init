#ifndef EDINIT_AST_STATEMENT_HPP
#define EDINIT_AST_STATEMENT_HPP

#include "Object.hpp"

namespace edinit::ast {

class Statement:
    public Object
{
public:
    ~Statement() override = 0;

    auto clone() -> Statement * override = 0;
    auto children() -> Children override;

protected:
    Statement();
    Statement(const Statement & other);
    Statement(Statement && other) noexcept;
    auto swap(Statement & other) noexcept -> void;

private:
    auto operator =(const Statement & other) -> Statement & = delete;
    auto operator =(Statement && other) noexcept -> Statement & = delete;
};

using StatementPtr = std::shared_ptr<Statement>;

} // namespace edinit::ast

#endif
