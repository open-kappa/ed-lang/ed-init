#ifndef EDINIT_AST_STRING_HPP
#define EDINIT_AST_STRING_HPP

#include "Value.hpp"
#include <string>

namespace edinit::ast {

class String final:
    public Value
{
public:
    String();
    ~String() override;
    String(const String & other);
    String(String && other) noexcept;
    auto operator =(const String & other) -> String &;
    auto operator =(String && other) noexcept -> String &;
    auto swap(String & other) noexcept -> void;

    auto value(const std::string & value) noexcept -> void;
    auto value() const noexcept -> const std::string &;

    // @TODO
    // add parts
    // as c string

private:
    std::string _value;
};

auto swap(String & i1, String & i2) noexcept -> void;

using StringPtr = std::shared_ptr<String>;

} // namespace edinit::ast

#endif
