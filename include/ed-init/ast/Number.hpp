#ifndef EDINIT_AST_NUMBER_HPP
#define EDINIT_AST_NUMBER_HPP

#include "Value.hpp"
#include <string>

namespace edinit::ast {

enum class NumberBase
{
    DECIMAL,
    BINARY,
    OCTAL,
    HEXADECIMAL
};


enum class NumberFormat
{
    INTEGRAL,
    REAL
};

class Number final:
    public Value
{
public:
    Number();
    ~Number() override;
    Number(const Number & other);
    Number(Number && other) noexcept;
    auto operator =(const Number & other) -> Number &;
    auto operator =(Number && other) noexcept -> Number &;
    auto swap(Number & other) noexcept -> void;

    auto clone() -> Number * override;
    auto acceptPreVisitor(Visitor & visitor) -> VisitExit override;
    auto acceptPostVisitor(Visitor & visitor) -> VisitExit override;
    auto children() -> Children override;

    auto value(const std::string & value) noexcept -> void;
    auto value() const noexcept -> const std::string &;

    auto base(const NumberBase value) noexcept -> void;
    auto base() const noexcept -> NumberBase;

    auto format(NumberFormat value) noexcept -> void;
    auto format() const noexcept -> NumberFormat;

    // @TODO
    // isSigned -- maybe useless, only unsigned...
    // toUint64, to Int64, toDouble, toFloat, ...
    // plus, minus, mult, div, ...

private:
    std::string _value;
    NumberBase _base;
    NumberFormat _format;
};

auto swap(Number & i1, Number & i2) noexcept -> void;

using NumberPtr = std::shared_ptr<Number>;

} // namespace edinit::ast

#endif
