#ifndef EDINIT_AST_TEMPLATEPARAMETERASSIGN_HPP
#define EDINIT_AST_TEMPLATEPARAMETERASSIGN_HPP

#include "ParameterAssign.hpp"

namespace edinit::ast {

class TemplateParameterAssign final:
    public Object
{
public:
    TemplateParameterAssign();
    ~TemplateParameterAssign() override;
    TemplateParameterAssign(const TemplateParameterAssign & other);
    TemplateParameterAssign(TemplateParameterAssign && other) noexcept;
    auto operator =(const TemplateParameterAssign & other) -> TemplateParameterAssign &;
    auto operator =(TemplateParameterAssign && other) noexcept -> TemplateParameterAssign &;
    auto swap(TemplateParameterAssign & other) noexcept -> void;

    auto parameters(const ParameterAssignList & value) -> void;
    auto parameters() const noexcept -> ParameterAssignList &;

private:
    ParameterAssignList _parameters;
};

auto swap(TemplateParameterAssign & i1, TemplateParameterAssign & i2) noexcept -> void;

using TemplateParameterAssignPtr = std::shared_ptr<TemplateParameterAssign>;

using TemplateParameterAssignList = List<TemplateParameterAssign>;

} // namespace edinit::ast

#endif
