#ifndef EDINIT_AST_SCOPE_HPP
#define EDINIT_AST_SCOPE_HPP

#include "Value.hpp"
#include <string>

namespace edinit::ast {

enum class ScopeOrder
{
    ORDERED,
    SEMIORDERED,
    UNORDERED
};


class Scope final:
    public Object
{
public:
    Scope();
    ~Scope() override;
    Scope(const Scope & other);
    Scope(Scope && other) noexcept;
    auto operator =(const Scope & other) -> Scope &;
    auto operator =(Scope && other) noexcept -> Scope &;
    auto swap(Scope & other) noexcept -> void;

    auto order(const ScopeOrder value) noexcept -> void;
    auto order() const noexcept -> ScopeOrder;

    // @TODO
    // add/remove

private:
    ScopeOrder _order;
};

auto swap(Scope & i1, Scope & i2) noexcept -> void;

using ScopePtr = std::shared_ptr<Scope>;

} // namespace edinit::ast

#endif
