#ifndef EDINIT_AST_OBJECT_HPP
#define EDINIT_AST_OBJECT_HPP

#include "List.hpp"
#include "Visitor.hpp"
#include <memory>
#include <string>
#include <vector>

namespace edinit::ast {

struct SourceInfo
{
    std::string file;
    size_t line;
    size_t column;
};

class Object
{
public:
    using ObjectPtr = std::shared_ptr<Object>;
    using Children = std::vector<ObjectPtr>;

    virtual ~Object() = 0;

    auto sourceInfo(const SourceInfo & info) -> void;
    auto sourceInfo() const noexcept -> const SourceInfo &;

    virtual auto clone() -> Object * = 0;

    virtual auto acceptPreVisitor(Visitor & visitor) -> VisitExit = 0;
    virtual auto acceptPostVisitor(Visitor & visitor) -> VisitExit = 0;
    virtual auto children() -> Children;

    static auto createdObjects() noexcept -> size_t;

protected:
    Object();
    Object(const Object & other);
    Object(Object && other) noexcept;
    auto swap(Object & other) noexcept -> void;

    SourceInfo _sourceInfo;

private:
    static size_t _createdObjects;

    auto operator =(const Object & other) -> Object & = delete;
    auto operator =(Object && other) noexcept -> Object & = delete;
};

using ObjectPtr = std::shared_ptr<Object>;
using ObjectList = List<Object>;

} // namespace edinit::ast

#endif
