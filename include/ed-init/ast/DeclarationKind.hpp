#ifndef EDINIT_AST_DECLARATIONKIND_HPP
#define EDINIT_AST_DECLARATIONKIND_HPP

namespace edinit::ast {

enum class DeclarationKind
{
    NONE,
    LET,
    DEF,
    CONST,
    MUT,
    VAR,
    ALIAS
};

} // namespace edinit::ast

#endif
