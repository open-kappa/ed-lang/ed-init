#include "../../include/ed-init/flex/Flex.hpp"

#include "../../include/ed-init/ast.hpp" // IWYU pragma: keep
#include "../../include/ed-init/parser.hpp" // IWYU pragma: keep

typedef void * yyscan_t;

#include "../../build/bison.hpp" // IWYU pragma: keep

#include "../../build/flex.hpp"

#include <cstdio>
#include <exception>

#ifdef __GNUC__
    #pragma GCC diagnostic ignored "-Wc++20-extensions"
#endif

namespace edinit::flex {

Flex::Flex():
    _flex(nullptr)
{
    const auto err = yylex_init(&_flex);
    if (err != 0) throw std::exception();
}

Flex::~Flex() noexcept
{
    clear();
}

auto Flex::tokenizeFile(const std::string & file) -> std::vector<FlexToken>
{
    // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    auto * const filePtr = fopen(file.c_str(), "re");
    const auto err0 = fseek(filePtr, 0, SEEK_END);
    if (err0 != 0)
    {
        // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
        (void) fclose(filePtr);
        throw std::exception();
    }
    const auto size = ftell(filePtr);
    if (size < 0)
    {
        // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
        (void) fclose(filePtr);
        throw std::exception();
    }

    const auto err1 = fseek(filePtr, 0, SEEK_SET);
    if (err1 != 0)
    {
        // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
        (void) fclose(filePtr);
        throw std::exception();
    }

    // NOLINTNEXTLINE(*-avoid-c-arrays))
    #ifdef __GNUC__
        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wvla"
    #endif
    // NOLINTNEXTLINE(*-avoid-c-arrays)
    char str[size_t(size + 1)];
    #ifdef __GNUC__
        #pragma GCC diagnostic pop
    #endif
    const auto readSize = fread(static_cast<char *>(str), 1, size_t(size), filePtr);
    const auto isReadError = ferror(filePtr) != 0;
    str[size_t(size)] = '\0';

    // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    (void) fclose(filePtr);

    if (isReadError)
    {
        throw std::exception();
    }
    if (readSize != size_t(size))
    {
        throw std::exception();
    }

    return tokenizeString(static_cast<char *>(str), file);
}

auto Flex::tokenizeString(
    const std::string & str,
    const std::string & file
) -> std::vector<FlexToken>
{
    // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    auto * const bufferState = yy_scan_string(str.c_str(), _flex);
    if (bufferState == nullptr)
    {
        throw std::exception();
    }

    std::vector<FlexToken> ret;
    for (;;)
    {
        YYSTYPE valueParam = {};
        YYLTYPE locationParam = {};
        const auto token = yylex(&valueParam, &locationParam, _flex);
        if (token == 0) break;
        FlexToken tk = {
            .value = yyget_text(_flex),
            .file = file,
            .line = size_t(yyget_lineno(_flex)),
            .column = size_t(yyget_column(_flex))
        };
        ret.push_back(tk);
    }

    yy_delete_buffer(bufferState, _flex);
    return ret;
}

auto Flex::clear() noexcept -> void
{
    if (_flex == nullptr) return;
    (void) yylex_destroy(_flex);
    _flex = nullptr;
}

} // namespace edinit::flex
