#include "../../include/ed-init/ast/Value.hpp"

namespace edinit::ast {

Value::~Value()
{}

auto Value::children() -> Children
{
    return Object::children();
}

Value::Value():
    Object()
{}

Value::Value(const Value & other):
    Object(other)
{}

Value::Value(Value && other) noexcept:
    Object(std::move(other))
{}

auto Value::swap(Value & other) noexcept -> void
{
    using std::swap;
    Object::swap(other);
}

} // namespace edinit::ast
