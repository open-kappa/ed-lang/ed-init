#include "../../include/ed-init/ast/Statement.hpp"

namespace edinit::ast {

Statement::~Statement()
{}

auto Statement::children() -> Children
{
    return Object::children();
}

Statement::Statement():
    Object()
{}

Statement::Statement(const Statement & other):
    Object(other)
{}

Statement::Statement(Statement && other) noexcept:
    Object(std::move(other))
{}

auto Statement::swap(Statement & other) noexcept -> void
{
    using std::swap;
    Object::swap(other);
}

} // namespace edinit::ast
