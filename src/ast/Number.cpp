#include "../../include/ed-init/ast/Number.hpp"

namespace edinit::ast {

Number::Number():
    Value(),
    _value(),
    _base(NuberBase::DECIMAL),
    _format(NumberFormat::INTEGRAL)
{}

Number::~Number()
{}

Number::Number(const Number & other):
    Value(other),
    _value(other._value),
    _base(other._base),
    _format(other._format)
{}

Number::Number(Number && other) noexcept:
    Value(std::move(other)),
    _value(std::move(other._value)),
    _base(other._base),
    _format(other._format)
{}

auto Number::operator =(const Number & other) -> Number &
{
    Number tmp(other);
    swap(tmp);
    return *this;
}

auto Number::operator =(Number && other) noexcept -> Number &
{
    Number tmp(std::move(other));
    swap(tmp);
    return *this;
}

auto Number::swap(Number & other) noexcept -> void
{
    using std::swap;
    Value::swap(other);
    swap(_value, other._value);
    swap(_base, other._base);
    swap(_format, other._format);
}

auto Number::clone() -> Number *
{
    // NOLINTNEXTLINE(*-owning-memory)
    return new Number(*this);
}

auto Number::acceptPreVisitor(Visitor & visitor) -> VisitExit
{
    visitor.preVisit(*this);
}

auto Number::acceptPostVisitor(Visitor & visitor) -> VisitExit
{
    visitor.postVisit(*this);
}

auto Number::children() -> Children
{
    return Value::children();
}

auto Number::value(const std::string & value) noexcept -> void
{
    _value = value;
}

auto Number::value() const noexcept -> const std::string &
{
    return _value;
}

auto Number::base(const NumberBase value) noexcept -> void
{
    _base = value;
}

auto Number::base() const noexcept -> NumberBase
{
    return _base;
}

auto Number::format(const NumberFormat value) noexcept -> void
{
    _format = value;
}

auto Number::format() const noexcept -> NumberFormat
{
    return _format;
}

} // namespace edinit::ast
