#include "../../include/ed-init/ast/Identifier.hpp"

namespace edinit::ast {

Identifier::Identifier():
    Value(),
    _name(),
    _prefix()
{}

Identifier::~Identifier()
{}

Identifier::Identifier(const Identifier & other):
    Value(other),
    _name(other._name),
    _prefix(other._prefix->clone())
{}

Identifier::Identifier(Identifier && other) noexcept:
    Value(std::move(other)),
    _name(std::move(other._name)),
    _prefix(std::move(other._prefix))
{}

auto Identifier::operator =(const Identifier & other) -> Identifier &
{
    Identifier tmp(other);
    swap(tmp);
    return *this;
}

auto Identifier::operator =(Identifier && other) noexcept -> Identifier &
{
    Identifier tmp(std::move(other));
    swap(tmp);
    return *this;
}

auto Identifier::swap(Identifier & other) noexcept -> void
{
    using std::swap;
    Value::swap(other);
    swap(_name, other._name);
    swap(_prefix, other._prefix);
}

auto Identifier::clone() -> Identifier *
{
    // NOLINTNEXTLINE(*-owning-memory)
    return new Identifier(*this);
}

auto Identifier::acceptPreVisitor(Visitor & visitor) -> VisitExit
{
    visitor.preVisit(*this);
}

auto Identifier::acceptPostVisitor(Visitor & visitor) -> VisitExit
{
    visitor.postVisit(*this);
}

auto Identifier::children() -> Children
{
    auto && ret = Value::children();
    ret.push_back(_prefix);
    return ret;

}

auto Identifier::name(const std::string & value) noexcept -> void
{
    _name = value;
}

auto Identifier::name() const noexcept -> const std::string &
{
    return _name;
}

auto Identifier::prefix(const ValuePtr & value) noexcept -> void
{
    _prefix = value;
}

auto Identifier::prefix() const noexcept -> ValuePtr
{
    return _prefix;
}


} // namespace edinit::ast
