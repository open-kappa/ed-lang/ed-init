#include "../../include/ed-init/ast/Visitor.hpp"
#include "../../include/ed-init/ast.hpp" // IWYU pragma: keep
#include <vector>

namespace edinit::ast {
namespace /*anon*/ {
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpadded"
#endif
struct ObjOrFoo
{
    explicit ObjOrFoo(ObjectPtr obj_):
        obj(std::move(obj_)),
        isPre(true)
    {}

    ObjOrFoo(ObjectPtr obj_, const bool pre):
        obj(std::move(obj_)),
        isPre(pre)
    {}

    ObjectPtr obj;
    bool isPre;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
} // namespace

Visitor::Visitor()
{}

Visitor::~Visitor()
{}

auto Visitor::execDeepVisit(const ObjectPtr & root) -> void
{
    using List = std::vector<ObjOrFoo>;
    using DiffType = List::difference_type;
    List objs;
    objs.emplace_back(ObjOrFoo(root));

    // NOLINTNEXTLINE(modernize-loop-convert)
    for (auto it = objs.begin(); it != objs.end(); ++it)
    {
        auto obj = it->obj;
        const auto isPre = it->isPre;
        if (isPre)
        {
            const auto visitExit = obj->acceptPreVisitor(*this);
            switch (visitExit)
            {
                case VisitExit::CONTINUE:
                    break;
                case VisitExit::SKIP_CHILDREN:
                    continue;
                case VisitExit::STOP:
                    return;
            }
            auto && children = obj->children();
            objs.insert(it + 1, children.begin(), children.end());
            objs.insert(it + 1 + DiffType(children.size()), ObjOrFoo(obj, false));
        }
        else
        {
            const auto visitExit = obj->acceptPostVisitor(*this);
            switch (visitExit)
            {
                case VisitExit::CONTINUE:
                    break;
                case VisitExit::SKIP_CHILDREN:
                    // No sense, probably a bug...
                    break;
                case VisitExit::STOP:
                    return;
            }
        }
    }
}

auto Visitor::execBreadthVisit(const ObjectPtr & root) -> void
{
    using List = std::vector<ObjOrFoo>;

    List objs;
    objs.emplace_back(root);

    // NOLINTNEXTLINE(modernize-loop-convert)
    for (auto it = objs.begin(); it != objs.end(); ++it)
    {
        auto obj = it->obj;
        const auto isPre = it->isPre;
        if (isPre)
        {
            const auto visitExit = obj->acceptPreVisitor(*this);
            switch (visitExit)
            {
                case VisitExit::CONTINUE:
                    break;
                case VisitExit::SKIP_CHILDREN:
                    continue;
                case VisitExit::STOP:
                    return;
            }
            auto && children = obj->children();
            objs.insert(objs.end(), children.begin(), children.end());
            objs.insert(objs.end(), ObjOrFoo(obj, false));
        }
        else
        {
            const auto visitExit = obj->acceptPostVisitor(*this);
            switch (visitExit)
            {
                case VisitExit::CONTINUE:
                    break;
                case VisitExit::SKIP_CHILDREN:
                    // No sense, probably a bug...
                    break;
                case VisitExit::STOP:
                    return;
            }
        }
    }
}

auto Visitor::preVisit(Declaration & obj) -> VisitExit
{
    return preVisit(static_cast<Object &>(obj));
}

auto Visitor::preVisit(Identifier & obj) -> VisitExit
{
    return preVisit(static_cast<Value &>(obj));
}

auto Visitor::preVisit(Number & obj) -> VisitExit
{
    return preVisit(static_cast<Value &>(obj));
}

auto Visitor::preVisit(Object & /*obj*/) -> VisitExit
{
    return VisitExit::CONTINUE;
}

auto Visitor::preVisit(ParameterAssign & obj) -> VisitExit
{
    return preVisit(static_cast<Object &>(obj));
}

auto Visitor::preVisit(ParameterDeclaration & obj) -> VisitExit
{
    return preVisit(static_cast<Declaration &>(obj));
}

auto Visitor::preVisit(Scope & obj) -> VisitExit
{
    return preVisit(static_cast<Object &>(obj));
}

auto Visitor::preVisit(Statement & obj) -> VisitExit
{
    return preVisit(static_cast<Object &>(obj));
}

auto Visitor::preVisit(String & obj) -> VisitExit
{
    return preVisit(static_cast<Value &>(obj));
}

auto Visitor::preVisit(TemplateDeclaration & obj) -> VisitExit
{
    return preVisit(static_cast<Declaration &>(obj));
}

auto Visitor::preVisit(TemplateParameterAssign & obj) -> VisitExit
{
    return preVisit(static_cast<Object &>(obj));
}

auto Visitor::preVisit(Value & obj) -> VisitExit
{
    return preVisit(static_cast<Object &>(obj));
}

auto Visitor::postVisit(Declaration & obj) -> VisitExit
{
    return postVisit(static_cast<Object &>(obj));
}

auto Visitor::postVisit(Identifier & obj) -> VisitExit
{
    return postVisit(static_cast<Value &>(obj));
}

auto Visitor::postVisit(Number & obj) -> VisitExit
{
    return postVisit(static_cast<Value &>(obj));
}

auto Visitor::postVisit(Object & /*obj*/) -> VisitExit
{
    return VisitExit::CONTINUE;
}

auto Visitor::postVisit(ParameterAssign & obj) -> VisitExit
{
    return postVisit(static_cast<Object &>(obj));
}

auto Visitor::postVisit(ParameterDeclaration & obj) -> VisitExit
{
    return postVisit(static_cast<Declaration &>(obj));
}

auto Visitor::postVisit(Scope & obj) -> VisitExit
{
    return postVisit(static_cast<Object &>(obj));
}

auto Visitor::postVisit(Statement & obj) -> VisitExit
{
    return postVisit(static_cast<Object &>(obj));
}

auto Visitor::postVisit(String & obj) -> VisitExit
{
    return postVisit(static_cast<Value &>(obj));
}

auto Visitor::postVisit(TemplateDeclaration & obj) -> VisitExit
{
    return postVisit(static_cast<Declaration &>(obj));
}

auto Visitor::postVisit(TemplateParameterAssign & obj) -> VisitExit
{
    return postVisit(static_cast<Object &>(obj));
}

auto Visitor::postVisit(Value & obj) -> VisitExit
{
    return postVisit(static_cast<Object &>(obj));
}

} // namespace edinit::ast
