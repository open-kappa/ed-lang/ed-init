#include "../../include/ed-init/ast/Declaration.hpp"

namespace edinit::ast {

Declaration::~Declaration()
{}

auto Declaration::children() -> Children
{
    return Object::children();
}

Declaration::Declaration():
    Object()
{}

Declaration::Declaration(const Declaration & other):
    Object(other)
{}

Declaration::Declaration(Declaration && other) noexcept:
    Object(std::move(other))
{}

auto Declaration::swap(Declaration & other) noexcept -> void
{
    using std::swap;
    Object::swap(other);
}

} // namespace edinit::ast
