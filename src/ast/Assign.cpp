#include "../../include/ed-init/ast/Assign.hpp"

namespace edinit::ast {

Assign::Assign():
    Statement(),
    _source(),
    _target(),
    _op()
{}

Assign::~Assign()
{}

Assign::Assign(const Assign & other):
    Statement(other),
    _source(other._source->clone())
    _target(other._target->clone()),
    _op(other._op)
{}

Assign::Assign(Assign && other) noexcept:
    Statement(std::move(other)),
    _source(std::move(other._source)),
    _target(std::move(other._source)),
    _op(std::move(other._op))
{}

auto Assign::operator =(const Assign & other) -> Assign &
{
    Assign tmp(other);
    swap(tmp);
    return *this;
}

auto Assign::operator =(Assign && other) noexcept -> Assign &
{
    Assign tmp(std::move(other));
    swap(tmp);
    return *this;
}

auto Assign::swap(Assign & other) noexcept -> void
{
    using std::swap;
    Statement::swap(other);
    swap(_source, other._source);
    swap(_target, other._target);
    swap(_op, other._op);
}

auto Assign::clone() -> Assign *
{
    // NOLINTNEXTLINE(*-owning-memory)
    return new Assign(*this);
}

auto Assign::acceptPreVisitor(Visitor & visitor) -> VisitExit
{
    visitor.preVisit(*this);
}

auto Assign::acceptPostVisitor(Visitor & visitor) -> VisitExit
{
    visitor.postVisit(*this);
}

auto Assign::children() -> Children
{
    auto && ret = Statement::children();
    ret.push_back(_target);
    ret.push_back(_source);
    return ret;
}

auto Assign::source(const ValuePtr & value) -> void
{
    _source = value;
}

auto Assign::source() noexcept -> ValuePtr
{
    return _source;
}

auto Assign::target(const IdentifierPtr & value) -> void
{
    _target = value;
}

auto Assign::target() noexcept -> IdentifierPtr &
{
    return _target;
}

} // namespace edinit::ast
