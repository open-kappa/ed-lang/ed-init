#include "../../include/ed-init/ast/ParameterDeclaration.hpp"

namespace edinit::ast {

ParameterDeclaration::ParameterDeclaration():
    Declaration(),
    _defaultValue(),
    _type(),
    _name()
{}

ParameterDeclaration::~ParameterDeclaration()
{}

ParameterDeclaration::ParameterDeclaration(const ParameterDeclaration & other):
    Declaration(other),
    _defaultValue(other._defaultValue),
    _type(other._type),
    _name(other._name)
{}

ParameterDeclaration::ParameterDeclaration(ParameterDeclaration && other) noexcept:
    Declaration(std::move(other)),
    _defaultValue(std::move(other._defaultValue)),
    _type(std::move(other._type)),
    _name(std::move(other._name))
{}

auto ParameterDeclaration::operator =(const ParameterDeclaration & other) -> ParameterDeclaration &
{
    ParameterDeclaration tmp(other);
    swap(tmp);
    return *this;
}

auto ParameterDeclaration::operator =(ParameterDeclaration && other) noexcept -> ParameterDeclaration &
{
    ParameterDeclaration tmp(std::move(other));
    swap(tmp);
    return *this;
}

auto ParameterDeclaration::swap(ParameterDeclaration & other) noexcept -> void
{
    using std::swap;
    Declaration::swap(other);
    swap(_defaultValue, other._defaultValue);
    swap(_type, other._type);
    swap(_name, other._name);
}

auto ParameterDeclaration::clone() -> ParameterDeclaration *
{
    // NOLINTNEXTLINE(*-owning-memory)
    return new ParameterDeclaration(*this);
}

auto ParameterDeclaration::acceptPreVisitor(Visitor & visitor) -> VisitExit
{
    visitor.preVisit(*this);
}

auto ParameterDeclaration::acceptPostVisitor(Visitor & visitor) -> VisitExit
{
    visitor.postVisit(*this);
}

auto ParameterDeclaration::children() -> Children
{
    auto && ret = Declaration::children();
    ret.push_back(_defaultValue);
    ret.push_back(_type);
    return ret;
}

auto ParameterDeclaration::defaultValue(const ValuePtr & value) noexcept -> void
{
    _defaultValue = value;
}

auto ParameterDeclaration::defaultValue() noexcept -> ValuePtr &
{
    return _defaultValue;
}

auto ParameterDeclaration::type(const ValuePtr & value) noexcept -> void
{
    _type = value;
}

auto ParameterDeclaration::type() noexcept -> ValuePtr &
{
    return _type;
}

auto ParameterDeclaration::name(const std::string & value) -> void
{
    _name = value;
}

auto ParameterDeclaration::name() const noexcept -> const std::string &
{
    return _name;
}

} // namespace edinit::ast
