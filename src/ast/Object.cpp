#include "../../include/ed-init/ast/Object.hpp"

namespace edinit::ast {

Object::~Object()
{
    _createdObjects -= 1;
}

auto Object::sourceInfo(const SourceInfo & info) -> void
{
    _sourceInfo = info;
}

auto Object::sourceInfo() const noexcept -> const SourceInfo &
{
    return _sourceInfo;
}

auto Object::children() -> Children
{
    return Children();
}

auto Object::createdObjects() noexcept -> size_t
{
    return _createdObjects;
}

Object::Object():
    _sourceInfo()
{
    _createdObjects += 1;
}

Object::Object(const Object & other):
    _sourceInfo(other._sourceInfo)
{
    _createdObjects += 1;
}

Object::Object(Object && other) noexcept:
    _sourceInfo(std::move(other._sourceInfo))
{
    _createdObjects += 1;
}

auto Object::swap(Object & other) noexcept -> void
{
    using std::swap;
    swap(_sourceInfo, other._sourceInfo);
}

size_t Object::_createdObjects = 0;

} // namespace edinit::ast
