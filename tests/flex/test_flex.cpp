#include "../../include/ed-init.hpp"
#include <cstdlib>
#include <iostream>

using Flex = edinit::flex::Flex;

namespace /*anon*/ {

auto assert(const bool cond, const std::string msg) -> void
{
    if (cond) return;
    std::cerr << msg << std::endl;
    // NOLINTNEXTLINE(concurrency-mt-unsafe)
    exit(EXIT_FAILURE);
}

auto testEmpty(Flex & flex) -> void
{
    const auto ret = flex.tokenizeString("");
    assert(ret.empty(), "testEmpty: not empty");
}

auto testWhiteCharacters(Flex & flex) -> void
{
    const std::string chars = " \b\f\r\v";
    const auto ret = flex.tokenizeString(chars, "");
    assert(ret.size() != chars.size(), "testEmpty: not empty");
}

} // namespace

auto main() -> int
{
    Flex flex;

    testEmpty(flex);
    testWhiteCharacters(flex);

    return EXIT_SUCCESS;
}
